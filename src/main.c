#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
int main(int argc, char *argv[]) {
	srand (time (NULL));
	const char ALLOWED[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	char random[10+1];
	int i = 0;
	int c = 0;
	int nbAllowed = sizeof(ALLOWED)-1;
	for(i=0;i<10;i++) {
    	c = rand() % nbAllowed ;
	    random[i] = ALLOWED[c];
	};
	random[10] = '\0';
	if ( argc == 2 ) {
		printf("Checking key: %s\n", argv[1]);
		int sum = 0;
		for(int i = 0; i < strlen(argv[1]); i++) {
			sum += (int)argv[1][i];
		};
		if ( sum == 260 ) {
			printf("\033[0;32mOK\033[0m\n");
			printf("Your key: %s\n", random);
			sleep(1);
			return 0;
		} else {
			printf("\033[0;31mWrong.\033[0m\n");
			return 1;
		};
	} else {
		printf("USAGE: ./main <KEY>\n");
		return 1;
	};
	return 0;
};
